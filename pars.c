#include "fdf.h"

int		count_line(char *file)
{
	int		fd;
	char	buff;
	int		nb;

	nb = 0;
	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		perror(file);
		exit(0);
	}
	while (read(fd, &buff, 1))
	{
		if (buff == '\n')
			nb++;
	}
	close(fd);
	return (nb);
}
void free_temp(char **temp)
{
	int i;

	i = 0;
	while (temp[i])
		free(temp[i++]);
	free(temp);
}
void fill_array(int *array, char *data, t_env *env)
{
	char	**temp;
	int		i;

	i = 0;
	temp = ft_strsplit(data, ' ');
	while (temp[i])
	{
		array[i] = ft_atoi(temp[i]);
		if (array[i] > env->max)
			env->max = array[i];
		if (array[i] < env->min)
			env->min = array[i];
		i++;
	}
	free_temp(temp);
	array[i] = LIMIT;
}

int **get_map(char *file, t_env *env)
{
	int		**ret_val;
	int		fd;
	char	*line;
	int		l_number;
	int		i;

	l_number = count_line(file);
	env->max = 0;
	env->min = 0;
	ret_val = (int **)malloc(sizeof(int *) * (l_number + 100));
	ret_val[l_number] = NULL;
	fd = open(file, O_RDONLY);
	i = 0;
	while (get_next_line(fd, &line) > 0 && i < l_number)
	{
		ret_val[i] = (int *)ft_memalloc(sizeof(int) * (ft_strlen(line)/2 + 2));
		fill_array(ret_val[i], line, env);
		i++;
		free(line);
	}
	return (ret_val);
}
/*
int main()
{
	int **map;
	int i,j;

	i = 0;
	map = get_map("pylone.fdf");
	while (map[i] != NULL)
	{
		j = 0;
		while (map[i][j] != LIMIT)
			printf("%d ", map[i][j++]);
		printf("\n");
		i++;
	}
}
*/
