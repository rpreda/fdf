#ifndef FT_FDF_H
# define FT_FDF_H

# include <stdio.h>
# include <mlx.h>
# include <math.h>
# include <fcntl.h>

# include "get_next_line.h"
# include "libft.h"

# define CTE 0.91
# define CTE2 0.51
# define LIMIT 2000000000
# define CORDS_RATIO1 24	//3 and 2 also work
# define CORDS_RATIO2 12
# define CORDS_RATIO 25
# define H_RATIO 2			//200 switch
# define DEPTH_OFFSET 2
# define Y_RATIO 2
# define PRINT_Y_OFFSET 100
# define PRINT_X_OFFSET 100
# define COL 4947200		//16777215//11174400
# define SGN(x) ((x < 0) ? - 1 : ((x > 0) ? 1 : 0))

typedef struct	s_point3d
{
	int x;
	int y;
	int z;
}				t_point3d;

typedef struct	s_point2d
{
	int x;
	int y;
	int val;
}				t_point2d;

typedef struct	s_env
{
	void	*mlx;
	void	*win;
	int		**map;
	int		min;
	int		max;
	int		argc;
}				t_env;

t_env			*parse(char *file);
int				**get_map(char *f, t_env *env);
int				key_hook(int keycode, t_env *ptr);
int				expose_hook(t_env *pointer);
t_point2d		get_p2d(int i, int j, int value);
void			line(t_point2d p1, t_point2d p2, float col, t_env *env);
t_point2d		project_paral(t_point3d p);
void			draw_map(t_env *env);
void			get_point(t_point2d *coord, int x, int y, int val);
t_point2d		project_iso(t_point3d point);

#endif
