#include <mlx.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "libft.h"
#include "fdf.h"
#include <time.h>
#include <unistd.h>
#define CTE 0.61
#define CTE2 0.6
#define sgn(x) ((x<0)?-1:((x>0)?1:0))
void line_fast(t_point2d p1, t_point2d p2, int color, void *mlx_ptr, void *win_ptr);
t_point2d convert_point(t_point3d point);
t_point2d gen_point(int i, int j, int value)
{
	t_point3d temp;

	temp.x = j * 25;
	temp.z = i * 25 + 20;
	temp.y = -value * 4;
	return (convert_point(temp));
}
void draw_map(t_env *env)
{
	int i;
	int j;

	mlx_clear_window(env->mlx,env->win);
	printf("%denvr\n", env->max_rows);
	for (i = 0; i < env->max_rows - 1; i++)
	{
		for (j = 0; j<8; j++)
		{
			if (j > 0)
				line_fast(gen_point(i, j, env->matrix[i][j]), gen_point(i, j-1, env->matrix[i][j-1]), 16750230 , env->mlx, env->win);
			if (i > 0)	
				line_fast(gen_point(i, j, env->matrix[i][j]), gen_point(i - 1, j, env->matrix[i - 1][j]), 16750230 , env->mlx, env->win);
			if (j < 19)
				line_fast(gen_point(i, j, env->matrix[i][j]), gen_point(i, j + 1, env->matrix[i][j + 1]), 16750230 , env->mlx, env->win);
			if (i < 19)
				line_fast(gen_point(i, j, env->matrix[i][j]), gen_point(i + 1, j, env->matrix[i + 1][j]), 16750230 , env->mlx, env->win);
		}
	}
}
int exit_hook(int keycode, void *ptr)
{
	printf("%d KEYCODE\n", keycode);
	if (keycode == 65307)
		exit(0);
	return (0);
}
t_point2d convert_point(t_point3d point)
{
	t_point2d ret_val;
	ret_val.x = point.x + CTE * point.z;
	ret_val.y = point.y + CTE / 2 * point.z;
	return (ret_val);
} //parallel projection
/*
t_point2d convert_point(t_point3d point)
{
	t_point2d ret_val;
	ret_val.x = CTE * point.x - CTE2 * point.y;
	ret_val.y = point.z + CTE / 2 * point.x + CTE2 / 2 * point.y;
	return (ret_val);
}*/
int main(int argc, char **argv)
{
	void *mlx_pointer;
	void *window;
	t_env *env;
	env = (t_env *)malloc(sizeof(t_env));
	t_point3d p1,p2,p3,p4;
	t_point2d p_1,p_2,p_3,p_4;
	mlx_pointer = mlx_init();
	window = mlx_new_window(mlx_pointer, 800, 800, "TEST_PROGRAM");
	env->win = window;
	env->mlx = mlx_pointer;
	get_map(argv[1], env);
	draw_map(env);
	mlx_key_hook(window, exit_hook, env);
	mlx_loop(mlx_pointer);
	return (0);
}


void line_fast(/*int x1, int y1, int x2, int y2*/t_point2d p1, t_point2d p2, int color, void *mlx_ptr, void *win_ptr)
{
	int i,dx,dy,sdx,sdy,dxabs,dyabs,x,y,px,py,J;

	p1.y +=200;
	p2.y +=200;
	dx=p2.x-p1.x;      /* the horizontal distance of the line */
	dy=p2.y-p1.y;      /* the vertical distance of the line */
	dxabs=abs(dx);
	dyabs=abs(dy);
	sdx=sgn(dx);
	sdy=sgn(dy);
	x=dyabs>>1;
	y=dxabs>>1;
	px=p1.x;
	py=p1.y;

	//printf("x%dy%d-x%dy%d, ", p1.x, p1.y, p2.x, p2.y);
	if (dxabs>=dyabs) /* the line is more horizontal than vertical */
	{
		for(i=0;i<dxabs;i++)
		{
			y+=dyabs;
			if (y>=dxabs)
			{
				y-=dxabs;
				py+=sdy;
			}
			px+=sdx;
			mlx_pixel_put(mlx_ptr,win_ptr,px,py,color);
		}
	}
	else /* the line is more vertical than horizontal */
	{
		for(i=0;i<dyabs;i++)
		{
			x+=dxabs;
			if (x>=dyabs)
			{
				x-=dyabs;
				px+=sdx;
			}
			py+=sdy;
			mlx_pixel_put(mlx_ptr,win_ptr,px,py,color);
		}
	}
}
