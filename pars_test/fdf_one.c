void free_matrix(char **matrix)
{
	char **temp;

	temp = matrix;
	while(*matrix)
		free(*matrix);
	free(temp);
}
void get_limits(char *file, t_env *env)
{
	int		fd;
	char	*line;
	int		max;
	int		temp;

	fd = 0;
	max = 0;
	fd = open(file, O_RDONLY);
	if (fd <= 0)
	{
		perror("Usage ./fdf map_file");
		exit(1);
	}
	while (get_next_line(fd, &line))
	{
		if ((temp = ft_strlen(line)) > max)
			max = temp;
		env->max_rows += 1;
	}
	close(fd);
	env->max_columns = max;
}
void get_matrix(char *file, t_env *env)
{
	int		i;
	int		j;
	int		fd;
	char	*line;
	char	**temp;

	env->matrix = (int **)ft_memalloc(sizeof(int *) * env->max_rows);
	i = 0;
	j = 0;
	while (i < env->max_rows)
		env->matrix[i++] = (int *)ft_memalloc(sizeof(int) * env->max_columns);
	fd = open(file, O_RDONLY);
	while (get_next_line(fd, &line))
	{
		i = 0;
		temp = ft_strsplit(line, ' ');
		while (temp[i])
		{
			env->matrix[j][i] = ft_atoi(temp[i]);
			i++;
		}
		env->max_columns = i;
		free(temp);
		free(line);
		j++;
	}
	env->max_rows = j;
	close(fd);
}
t_env *parse(char *file)
{
	t_env *ret_val;

	ret_val = (t_env *)ft_memalloc(sizeof(t_env));
	get_limits(file, ret_val);
	get_matrix(file, ret_val);
	return (ret_val);
}
int main()
{
	int i,j;
	t_env *env;
	env = parse("file");
	for (i=0; i<env->max_rows; i++)
	{
		for (j=0;j<env->max_columns;j++)
			printf("%d ", env->matrix[i][j]);
		printf("\n");
	}
	return (0);
}
