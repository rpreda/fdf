#include "fdf.h"

t_point2d	get_p2d(int i, int j, int value)
{
	t_point2d temp;

	get_point(&temp, j, i, value);
	return (temp);
}

void		get_point(t_point2d *coord, int x, int y, int val)
{
	coord->x = x - y;
	coord->y = x + y;
	coord->val = val;
	coord->y -= val * 0.2; //0.15
	coord->x *= CORDS_RATIO1;
	coord->y *= CORDS_RATIO2;
	coord->x += 470;
	coord->y -= 50;
}

t_point2d	project_paral(t_point3d point)
{
	t_point2d ret_val;

	ret_val.x = point.x + CTE * point.z;
	ret_val.y = point.y + CTE / 2 * point.z;
	return (ret_val);
}

t_point2d	project_iso(t_point3d point)
{
	t_point2d ret_val;

	ret_val.x = CTE * point.x - CTE2 * point.y;
	ret_val.y = point.z / 0.8 + CTE / 2 * point.x + CTE2 / 2 * point.y;
	ret_val.x += PRINT_X_OFFSET;
	return (ret_val);
}
