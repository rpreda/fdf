#include "fdf.h"

int	key_hook(int keycode, t_env *env)
{
	if (keycode == 65307)
	{
		mlx_destroy_window(env->mlx, env->win);
		exit(0);
	}
	return (0);
}
int expose_hook(t_env *env)
{
	(void)env;
	draw_map(env);
	return (0);
}
