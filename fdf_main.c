#include "fdf.h"

int max(int a, int b)
{
	if (a > b)
		return (a);
	return (b);
}

int min(int a, int b)
{
	if (a < b)
		return (a);
	return (b);
}

void draw_map(t_env *env)
{
	int i;
	int j;
	int prev_len;

	i = 0;
	j = 0;
	while (env->map[i] != NULL)
	{
		prev_len = j;
		j = 0;
		while (env->map[i][j] != LIMIT)
		{
			if (j > 0)
				line(get_p2d(i, j, env->map[i][j]), get_p2d(i, j - 1, env->map[i][j - 1]), COL, env);
			if (j < prev_len)
				line(get_p2d(i, j, env->map[i][j]), get_p2d(i - 1, j, env->map[i - 1][j]), COL, env);
			if ((j < prev_len + 1) && (prev_len - 1 > 0) && i != 0 && j != 0 && env->argc == 3)
				line(get_p2d(i, j, env->map[i][j]), get_p2d(i - 1, j - 1, env->map[i - 1][j - 1]), COL, env);
			j++;
		}
		i++;
	}
}

t_points	ft_struct(t_point2d p1, t_point2d p2)
{
	t_points	new;

	new.p1 = p1;
	new.p2 = p2;
	return (new);
}

void line(t_point2d p1, t_point2d p2, float rgb, t_env *env)
{
	/*int i;
	int dx;
	int dy;
	int sdx;
	int sdy;
	int dxabs;
	int dyabs;
	int x;
	int	y;
	int px;
	int py;*/

	/*int color_end;
	int color_start;
	float color_unit;
	int color_offset;
	float color_multiplier;*/

	t_line		var;
	t_color		color;

	ft_color(&color, &rgb, env, ft_struct(p1, p2);
	/*color.unit = 255 / (env->max - env->min + 0.0001);
	if (p1.val == p2.val)
	{
		rgb += (p1.val - env->min) * color.unit;
		color.offset = 0;
	}
	else
	{
		if (p1.val > p2.val)
		{
			color.start = (p2.val - env->min) * color.unit;
			color.end = color.start + (p1.val - p2.val) * color.unit;
		}
		else
		{
			color.start = (p1.val - env->min) * color_unit;
			color.end = color_start + (p2.val - p1.val) * color_unit;
		}
		color_offset = color_end - color_start;
	}*/

	/*p1.y += PRINT_Y_OFFSET;
	p2.y += PRINT_Y_OFFSET;
	dx = p2.x - p1.x;
	dy = p2.y - p1.y;
	dxabs = abs(dx);
	dyabs = abs(dy);
	sdx = SGN(dx);
	sdy = SGN(dy);
	x = dyabs >> 1;
	y = dxabs >> 1;
	px = p1.x;
	py = p1.y;*/
	ft_assign(&var, p1, p2);

	if (dxabs >= dyabs)
	{
		color_multiplier = color_offset / (dxabs + 0.000001);
		i = 0;
		while (i < dxabs)
		{
			y += dyabs;
			if (y >= dxabs)
			{
				y -= dxabs;
				py += sdy;
			}
			px += sdx;
			mlx_pixel_put(env->mlx, env->win, px, py, (int)color);
			color += color_multiplier;
			i++;
		}
	}
	else
	{
		color_multiplier = color_offset / (dyabs + 0.000001);
		i = 0;
		while (i < dyabs)
		{
			x += dxabs;
			if (x >= dyabs)
			{
				x -= dyabs;
				px += sdx;
			}
			py += sdy;
			mlx_pixel_put(env->mlx, env->win, px, py, (int)color);
			color += color_multiplier;
			i++;
		}
	}
}

int main(int argc, char **argv)
{
	t_env env;

	if (argc > 1)
	{
		env.mlx = mlx_init();
		env.win = mlx_new_window(env.mlx, 1920, 1080, "FDF");
		env.map = get_map(argv[1], &env);
		env.argc = argc;
		mlx_key_hook(env.win, key_hook, &env);
		mlx_expose_hook(env.win, expose_hook, &env);
		mlx_loop(env.mlx);
	}
	else
		ft_putstr("Usage: ./fdf file1\n");
	return (0);
}
